## Индивидуальный план Кузнецова Е.Д.:

| **Задача**                                                                                          | **Время в а.ч.** |
|-----------------------------------------------------------------------------------------------------|------------------|
| Создание загрузочного USB-диска и образов ВМ с установленным дистрибутивом KOMRAD SIEM              | 3 часа           |
| Создание и ведение Git репозитория                                                                  | 3 часа           |
| Управление проектом и коммуникации с партнерами                                                     | 4 часа           |
| Изучение теоретического материала по эксплуатации KOMRAD SIEM                                       | 10 часов         |
| Создание учебно-методического пособия, поиск материалов                                             | 8 часов          |
| Практическое применение KOMRAD SIEM в рамках лабораторных работ                                     | 6 часов          |
| Настройка и работа с оборудованием в лаборатории, установка  Сканер-ВС и создание учебного стенда   | 27 часов         |
| Форматирование и исправление документации                                                           | 4 часа           |
| Создание комплекта отчетной документации                                                            | 4 часа           |
| Исправление последних ошибок                                                                        | 3 часа           |
| Общее время работы в академических часах:                                                           | 72 часа          |
